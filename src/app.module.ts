import { Module } from '@nestjs/common';
import {MockServiceModule} from "./service/mock-service.module";
import {ScheduleModule} from "@nestjs/schedule";

@Module({
  imports: [
      MockServiceModule,
      ScheduleModule.forRoot(),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
