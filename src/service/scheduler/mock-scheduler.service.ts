import {Injectable, Logger} from "@nestjs/common";
import {MockDto, WebHookDefinitionDto} from "../mock/mock.dto";

@Injectable()
export class MockSchedulerService {

    private logger = new Logger('MockSchedulerService');

    private schedulerRepository = [];

    schedule(webhookDefinition: WebHookDefinitionDto) {
        if (webhookDefinition) {
            this.logger.log(`Scheduling webhook: ${JSON.stringify(webhookDefinition)}`);
            this.schedulerRepository.push(webhookDefinition);
        }
    }

    retrieveNextExecution(): WebHookDefinitionDto {
        return this.schedulerRepository.pop()
    }

    list(): WebHookDefinitionDto[] {
        this.logger.log(`Scheduler repository: ${this.schedulerRepository}`);
        return Array.from(this.schedulerRepository.values());
    }

    delete() {
        this.logger.log(`Scheduler repository: ${this.schedulerRepository}`);
        this.schedulerRepository = [];
    }
}
