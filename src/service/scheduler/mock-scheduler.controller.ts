import {Body, Controller, Delete, Get, Logger, Post, UsePipes, ValidationPipe} from "@nestjs/common";
import {MockSchedulerService} from "./mock-scheduler.service";


@Controller('scheduled-webhooks')
export class MockSchedulerController {

    private logger = new Logger('MockSchedulerController');

    constructor(private schedulerService: MockSchedulerService) { }

    // TODO not working
    @Get()
    listAll() {
        this.logger.log(`Listing scheduled webhooks`);
        return this.schedulerService.list();
    }

    // TODO not working
    @Delete()
    deleteAll() {
        this.logger.log(`Deleting scheduled webhooks`);
        return this.schedulerService.delete();
    }

}