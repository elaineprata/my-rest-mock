import {Module} from "@nestjs/common";
import {MockModule} from "../mock/mock.module";
import {MockSchedulerService} from "./mock-scheduler.service";
import {MockSchedulerController} from "./mock-scheduler.controller";
import {MockHttpWebhookService} from "../mock-webhook-service.service";

@Module({
    imports: [
        MockModule,
    ],
    controllers: [
        MockSchedulerController,
    ],
    providers: [
        MockSchedulerService,
    ],
})
export class MockSchedulerModule {

}
