import {Injectable, Logger} from "@nestjs/common";
import {ResponseDefinitionDto, ResponseDto, RuleConditionType, RuleDto} from "./mock/mock.dto";

export class Rule {
    private logger = new Logger('Rule');
    constructor(
        private conditionType: RuleConditionType,
        private fieldName: string,
        private value: string,
        private responseName: string,
    ) {
    }

    getResponseName(): string {
        return this.responseName;
    }

    apply(body: any): boolean {

        let apply = false;
        switch (this.conditionType) {
            case RuleConditionType.StringContains:
                apply = String(body[this.fieldName]).includes(this.value);
                break;
            case RuleConditionType.StringEquals:
                apply = String(body[this.fieldName]) == this.value;
                break;
            case RuleConditionType.NumberEquals:
                apply = Number(body[this.fieldName]) == Number(this.value);
                break;
            case RuleConditionType.NumberGreaterThan:
                apply = Number(body[this.fieldName]) > Number(this.value);
                break;
            case RuleConditionType.NumberLowerThan:
                apply = Number(body[this.fieldName]) < Number(this.value);
                break;
            case RuleConditionType.NumberGreaterOrEqualThan:
                apply = Number(body[this.fieldName]) >= Number(this.value);
                break;
            case RuleConditionType.NumberLowerOrEqualThan:
                apply = Number(body[this.fieldName]) <= Number(this.value);
                break;
            default:
                this.logger.log(`Rule type does not match`);
        }
        return apply;
    }
}

@Injectable()
export class MockResponseSolver {

    private logger = new Logger('MockResponseSolver');

    solveResponse(responseDefinition: ResponseDefinitionDto, body: any): ResponseDto {
        const dto = this.solveRules(responseDefinition.rules, responseDefinition.responses, body);
        if (dto) {
            return dto;
        }

        return this.solveDefaultResponse(responseDefinition.responses);
    }

    private solveRules(dtoRules: RuleDto[], responses: ResponseDto[], body: any): ResponseDto {
        this.logger.log(`Trying to solve response by rules`);

        if (dtoRules && dtoRules.length > 0) {

            const rules = dtoRules.map(r => new Rule(r.ruleConditionType, r.fieldName, r.value, r.responseName));
            const validRules = rules.filter(r => r.apply(body));

            if (validRules && validRules.length > 0) {
                const responseName = validRules[0].getResponseName();
                return responses.filter(r => r.name == responseName)[0];
            }
        }

        return;
    }

    private solveDefaultResponse(responses: ResponseDto[]): ResponseDto {
        this.logger.log(`Trying to solve default response`);

        const defaultResponse = responses.filter(response => response.default == true);
        if ((defaultResponse == undefined) || (defaultResponse.length == 0)) {
            // TODO exception
        }

        return defaultResponse[0];
    }
}
