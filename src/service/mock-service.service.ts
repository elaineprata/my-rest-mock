import {Injectable, Logger} from "@nestjs/common";
import {MockService} from "./mock/mock.service";
import {
    ResponseDto,
} from "./mock/mock.dto";
import {MockResponseSolver} from "./mock-response-service.service";
import {MockSchedulerService} from "./scheduler/mock-scheduler.service";

@Injectable()
export class MockServiceService {

    private logger = new Logger('MockServiceService');

    constructor(private mockService: MockService,
                private responseSolver: MockResponseSolver,
                private schedulerService: MockSchedulerService) { }

    handleService(serviceName: string, body: string): ResponseDto {
        const mock = this.mockService.findMockByName(serviceName);

        const responseDefinition = mock.responseDefinition;
        this.logger.log(`Response definition: ${JSON.stringify(responseDefinition)}`);

        const response = this.responseSolver.solveResponse(responseDefinition, body);
        this.logger.log(`Solved response: ${response}`);

        this.schedulerService.schedule(mock.webhookDefinition);

        return response;
    }

}