import {HttpModule, Module} from "@nestjs/common";
import {MockServiceController} from "./mock-service.controller";
import {MockServiceService} from "./mock-service.service";
import {MockModule} from "./mock/mock.module";
import {TaskService} from "./task.service";
import {MockResponseSolver} from "./mock-response-service.service";
import {MockSchedulerService} from "./scheduler/mock-scheduler.service";
import {MockHttpWebhookService, MockWebhookService} from "./mock-webhook-service.service";
import {MockSchedulerModule} from "./scheduler/mock-scheduler.module";

@Module({
    imports: [
        MockModule,
        MockSchedulerModule,
        HttpModule.register({
            timeout: 5000,
            maxRedirects: 5,
        }),
    ],
    controllers: [
        MockServiceController,
    ],
    providers: [
        MockServiceService,
        MockResponseSolver,
        MockWebhookService,
        MockHttpWebhookService,
        MockSchedulerService,
        TaskService,
    ],
})
export class MockServiceModule {

}
