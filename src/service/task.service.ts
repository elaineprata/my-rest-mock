import {Injectable, Logger} from "@nestjs/common";
import {Cron, CronExpression} from "@nestjs/schedule";
import {MockSchedulerService} from "./scheduler/mock-scheduler.service";
import {map} from "rxjs/operators";
import {MockWebhookService} from "./mock-webhook-service.service";

@Injectable()
export class TaskService {

    private logger = new Logger('TaskService');

    constructor(private schedulerService: MockSchedulerService, private webhookService: MockWebhookService) { }

    // @Cron(CronExpression.EVERY_HOUR)
    @Cron(CronExpression.EVERY_30_SECONDS)
    // @Cron(CronExpression.EVERY_5_MINUTES)
    run() {
        this.logger.log("Processing webhooks")

        // TODO configure number of webhooks to call each run
        const dto = this.schedulerService.retrieveNextExecution()
        if (dto) {
            this.webhookService.call(dto);
        }
    }
}
