import {Body, Controller, Get, Logger, Param, Post, UsePipes, ValidationPipe} from "@nestjs/common";
import {MockServiceService} from "./mock-service.service";
import {ResponseDto} from "./mock/mock.dto";
import {Cron, CronExpression} from "@nestjs/schedule";

@Controller('mock-services')
export class MockServiceController {

    private logger = new Logger('MockController');

    constructor(private mockService: MockServiceService) { }

    @Post('/:name')
    createMock(@Param('name')
                   name: string,
                @Body()
                   body: string): any {
        // https://docs.nestjs.com/controllers#request-object
        this.logger.log(`Post request received for mock service ${name} with body: ${JSON.stringify(body)}`);

        const parsedBody = JSON.parse(JSON.stringify(body));
        this.logger.log(`Body: ${parsedBody}`);

        // TODO handle response and response code
        const responseDto = this.mockService.handleService(name, body);
        const responseBody = responseDto.json.replace(/'/g, "\"");
        return JSON.parse(responseBody);
    }
}