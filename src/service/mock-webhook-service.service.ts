import {HttpService, Injectable, Logger} from "@nestjs/common";
import {HttpMethod, HttpWebHookDefinitionDto, WebHookDefinitionDto, WebhookType} from "./mock/mock.dto";
import {map} from "rxjs/operators";

@Injectable()
export class MockHttpWebhookService {
    private logger = new Logger('MockHttpWebhookService');

    constructor(private httpService: HttpService) { }

    call(dto: HttpWebHookDefinitionDto) {
        this.logger.log(`Calling HTTP webhook: ${dto.endpoint}`);

        // TODO webhook: allow to define some field in the request that will compose the webhook body
        // TODO webhook: rules to choose different bodies

        switch (dto.httpMethod) {
            case HttpMethod.GET:
                this.logger.log("GET");
                const get = this.httpService.get(dto.endpoint);
                const getS = get.pipe(
                    map(res => res.data))
                    .subscribe()
                this.logger.log(`Response: ${getS}`);
                break;
            case HttpMethod.POST:
                this.logger.log("POST");
                const body = (dto.json)?JSON.parse(dto.json.replace(/'/g, "\"")):"";
                const post = this.httpService.post(dto.endpoint, body);
                const postS = post.pipe(
                    map(res => res.data))
                    .subscribe()
                this.logger.log(`Response: ${postS}`);

                break;
            default:
                this.logger.log("No webhook called");

        }
    }
}

@Injectable()
export class MockWebhookService {

    private logger = new Logger('MockWebhookService');

    constructor(private httpWebhookService: MockHttpWebhookService) { }

    call(dto: WebHookDefinitionDto) {
        switch (dto.type) {
            case WebhookType.HTTP:
                this.httpWebhookService.call(dto as HttpWebHookDefinitionDto);
                break;
            default:
                this.logger.log("No webhook called");
        }

    }
}