import {Body, Controller, Delete, Get, Logger, Post, UsePipes, ValidationPipe} from "@nestjs/common";
import {MockDto} from "./mock.dto";
import {MockService} from "./mock.service";

@Controller('mocks')
export class MockController {

    private logger = new Logger('MockController');

    constructor(private mockService: MockService) { }

    @Post()
    @UsePipes(ValidationPipe)
    createMock(@Body()
                   body: MockDto) {

        this.logger.log(`Creating new mock: ${JSON.stringify(body)}`);

        this.logger.log(body.name);

        this.logger.log(body.responseDefinition);

        return this.mockService.createMock(body);
    }

    @Get()
    listAll() {
        this.logger.log(`Listing existing mocks`);
        return this.mockService.list();
    }

    @Delete()
    deleteAll() {
        this.logger.log(`Deleting existing mocks`);
        return this.mockService.delete();
    }

}