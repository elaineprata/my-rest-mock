import {Module} from "@nestjs/common";
import {MockController} from "./mock.controller";
import {MockService} from "./mock.service";

@Module({
    imports: [
    ],
    controllers: [
        MockController,
    ],
    providers: [
        MockService,
    ],
    exports: [
        MockService,
    ],
})

export class MockModule {

}

// TODO postman: request (rules, steps)
// TODO mock service

