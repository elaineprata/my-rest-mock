import {Injectable, Logger, NotFoundException} from "@nestjs/common";
import {MockDto} from "./mock.dto";

@Injectable()
export class MockService {

    private logger = new Logger('MockService');

    private mockRepository = new Map<string, MockDto>();

    // TODO duplicity validation
    createMock(request: MockDto): MockDto {
        this.mockRepository.set(request.name, request);
        return request;
    }

    findMockByName(name: string): MockDto {
        this.logger.log(`Searching for mock definition for service: ${name}`);

        if (!this.mockRepository.has(name)) {
            throw new NotFoundException(`Mock service with name ${name} not found.`);
        }

        this.logger.log(`Mock definition found for service ${name}: ${JSON.stringify(this.mockRepository.get(name))}`);
        return this.mockRepository.get(name);
    }

    list(): MockDto[] {
        return Array.from(this.mockRepository.values());
    }

    delete() {
        this.mockRepository = new Map<string, MockDto>();
    }
}
