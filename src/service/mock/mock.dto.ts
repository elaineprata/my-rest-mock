import {HttpStatus} from "@nestjs/common";
import {
    IsBoolean,
    IsNotEmpty,
    Validate, ValidateNested,
    ValidationArguments,
    ValidatorConstraint,
    ValidatorConstraintInterface
} from "class-validator";
import {Type} from "class-transformer";

@ValidatorConstraint({ name: "onlyOneDefaultResponse", async: false })
class OnlyOneDefaultResponseConstraint implements ValidatorConstraintInterface {

    validate(propertyValue: ResponseDto[]) {
        return propertyValue.filter(response => response.default == true).length <= 1;
    }

    defaultMessage(args: ValidationArguments) {
        return `"${args.property}" can have only one default response`;
    }
}

export enum RuleConditionType {
    StringContains = "stringContains",
    StringEquals = "stringEquals",
    NumberEquals = "numberEquals",
    NumberGreaterThan = "gt",
    NumberLowerThan = "lt",
    NumberGreaterOrEqualThan = "gte",
    NumberLowerOrEqualThan = "lte",
}

export class RuleDto {
    readonly fieldName: string;
    readonly ruleConditionType: RuleConditionType;
    readonly value: string;
    readonly responseName: string;
}

export class ResponseDto {
    @IsNotEmpty()
    readonly name: string;
    readonly json: string;
    readonly default: boolean;
    @IsNotEmpty()
    readonly status: HttpStatus;
}

export class ResponseDefinitionDto {
    @IsNotEmpty()
    @Validate(OnlyOneDefaultResponseConstraint)
    @ValidateNested()
    @Type(() => ResponseDto)
    readonly responses: ResponseDto[];
    readonly rules: RuleDto[];
}

export enum WebhookType {
    HTTP = "http",
}

export enum HttpMethod {
    GET = "get",
    POST = "post",
}

export class WebHookDefinitionDto {
    readonly type: WebhookType;
    constructor(type: WebhookType) { this.type = type; }
}

export class HttpWebHookDefinitionDto extends WebHookDefinitionDto {

    constructor() { super(WebhookType.HTTP) }

    readonly endpoint: string;
    readonly httpMethod: HttpMethod;
    readonly json: string;
}

export class MockDto {
    @IsNotEmpty()
    readonly name: string;

    @IsNotEmpty()
    @ValidateNested()
    @Type(() => ResponseDefinitionDto)
    readonly responseDefinition: ResponseDefinitionDto;

    readonly webhookDefinition: WebHookDefinitionDto;
}